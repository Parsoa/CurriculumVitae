The following document is in Markdown.

# Education
## B.S. in Software Engineering
Sharif University of Technology - Tehran, Iran

Mar. 2013 - Present

Will finish my studies by spring 2017. I currently hold a GPA of 16.95 (out of 20) which evaluates to 3.48 out of 4.

## Highschool (Diploma)
Allameh Helli Highschool for Exceptional Talents (NODET) - Tehran, Iran

Sep. 2008 - Jul. 2012

Graduated with a GPA of 19.2 (out of 20).

# Research

## Royan Stem Cell Technology

Tehran, Iran

Jun. 2016 - Present

Working on developing automated analysis pipelines for early detection of cancer based on clinical data; under supervision of Dr. Ali Sharifi Zarchi.

## HPCAN (High Performance Computers and Networks) Laboratory, Sharif University of Technology

Tehran, Iran

May. 2016 - Present

Performance study and analysis of CUDA GPU's shared memory system and possible improvement strategies. Supervised by Professor Hamid Sarbazi Azad.

# Teaching Assistance

## Data Structures & Fundamentals of Algorithms - Professor Mohammad Ghodsi

Four semesters. My responsibilities often included preparing written and programming assignments and teaching practice classes.

## Computer Networks - Professor Mehdi Kharrazi

Two semesters. Responsibilities included preparing programming assignments and teaching practice classes.

## Design of Algorithms - Dr Ali Sharifi Zarchi

One semester. I helped prepare some course material and exam questions.

## Numerical Methods - Professor Hamid Sarbazi Azad

One semester. I  prepared and scored written assignments.

## Fundamental of Programming in C - Dr Reza Entezari Maleki

Two semesters. I managed the course project and prepared quizzes and exams.

## Advanced Programming in Java - Peyman Dodange

Two semesters. I prepared and managed the course project and held programming workshops.

# Industrial Experience

## Nivad Cloud Services

Nivad is a startup providing various software back-end services and is funded by Sharif Accelerator (startup accelerator established by Sharif University of Technology). Developed a push notifications and mobile statistics framework as a part of Nivad's services.

## Padra Smart Security

Padra provides control and tracking software for a variety of car security hardware. I developed an Android application acting as a remote controller

## Padidar Smart Tracker

Padidar provides a range of location-aware services helping businesses with tracking and planning their assets and routines. My work mainly focused on the client Android application.

## Avanegar Messenger (formerly Chaapaarak)

Chaapaarak is a multi-platform messaging service designed for ease of use and speed. I mainly worked on user interface of the Android client application during the early months of the startup. During this period Chaapaarak received capital funding from Iraninan web giant Sarava.

# English Proficiency

## TOEFL

Total score of 115 with full marks in reading and listening sections (2016).

## Cambridge Teaching Knowledge Test (TKT)

Full mark (Band 4 out of 4) on all three modules (2013).

# Extracurricular Activity

## Sokhan Ashna Language Institute

Served as a substitution teacher for intermediate to high level english courses for two semesters. (Nov. 2012 - Sep. 2013)

## Music

I've been playing the piano since I was 12. I'm also familiar with the violin and received classical training with the instrument for two years.

Performed an instrumental version of song ''Careless Whisper'' (George Michael) during Sharif University's 2014 annual music festival.

# Honors

* 117th Place (among over a million competitors) in 2013 national university entrance examination (Konkur).

* Member of the National Elite Foundation, an establishment supporting those with significant educational or scientific achievements through various means.

# Program Committees

* Technical Staff, The 4th National Hardware Design Contest, Tehran University.

* Technical Staff, The 40th ACM International Collegiate Programming Contest, Asia Region, Tehran Site, Sharif University of Technology.

* Executive Staff, The 39th ACM International Collegiate Programming Contest, Asia Region, Tehran Site.

* Executive Staff, The 1st Java Challenge AI Contest, Department of Computer Engineering, Sharif University of Technology.
